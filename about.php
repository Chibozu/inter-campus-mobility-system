<!DOCTYPE html>
<html lang="en-us">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/lightcase.css">
    <link rel="stylesheet" href="assets/css/odometer.css">
    <link rel="stylesheet" href="assets/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/main.css">

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">

    <title>Check Mart | About Us</title>


</head>

<body>
   <!-- ==========Preloader========== -->
   <div class="preloader">
        <div class="preloader-wrapper">
            <img src="assets/css/loaders.gif" alt="car-loader">
        </div>
    </div>
    <!-- ==========Preloader========== -->

    <!-- ==========scrolltotop========== -->
    <a href="#0" class="scrollToTop" title="ScrollToTop">
        <img src="assets/images/rocket.png" alt="rocket">
    </a>
    <!-- ==========scrolltotop========== -->

    <!-- ==========header-section========== -->
    <?php include("inc/header.php"); ?>

    <!-- ===========Header Cart=========== -->
    <div id="body-overlay" class="body-overlay"></div>
    <!-- ==========hero-area========== -->
    <section class="hero-section">
        <div class="hero-area bg_img" data-background="assets/images/banner/checkmart-background.jpg">
            <div class="container">
                <h1 class="title">about us</h1>
            </div>
        </div>
        <div class="container">
            <ul class="breadcrumb">
                <li>
                    <a href="index.php">home</a>
                </li>
                <li>
                    about us
                </li>
            </ul>
        </div>
    </section>
    <section class="blog-section padding-bottom padding-top bg-ash">
        <div class="about-elements">
            <h4>Our Company</h4>
            <p>Checkmart Security and Travel Tours is an emerging Security and car rental Company that is owned by Local Zambians.  It was established to specifically deliver quality services to its customers and meet demanding market.
            Checkmart has the exposure and expertise necessary to provide guarding and car rental services to prestigious business houses, banks, departmental stores, industrial plants, call centres, school, offices, residential areas, filling stations etc.<br/><br/> 
            At the core of the company are professionals who are trusted with proven expertise and experience in the provision and management of comprehensive security and car hire solutions.<br/><br/>
            Our guards undergo top notch training which creates well rounded, prepared and skilled security officers who are proactive and stay calm under pressure. Training of our security staff at all levels forms the basis of the company assurance programme. 
            The guard service supported by 24/7 command control centre, mobile supervision and an extensive management support system which ensures that our clients receives the attention and responsiveness they deserve.
            </p>
            <h4>Our Aim</h4>
            <p>Waging peace, security and car hire services</p>
            <h4>Our Vision</h4>
            <p>To provide security and with cutting edge protection and loss prevention service unique in the field of security. We envision being the largest Zambian owned Security Company that provides customized Security solutions and not just 'manpower' or Security personnel to be utilized by the client and also providing the best car rental services.</p>
            <h4>Our Core Purpose</h4>
            <p>To offer wide range of services which provide most efficient and cost effective services for all kinds of corporate, small to medium size (SME'S) and individual clients.</p>
            <h4>Core Value</h4>
            <p>Checkmart fundamental core value are: - Integrity, Vigilance, Helpfulness and result oriented. These serve as guidance for all employees in building trust with customers, colleagues and surrounding communities.</p>
            <ol>
                <li>Integrity</li>
            </ol>                       
            <h4>Our Capabilities</h4>
            <p>At Checkmart, we take all matters seriously. No matter where we serve, we strive to be most responsive firm in the industry. Some of the services we offer include: -</p>
            <ol>
                <li>Guarding Services</li>
                <li>Car Hire Services</li>
                <li>Car Rental Services</li>
            </ol>
        </div>
    </section>
    <!-- ==========footer-section========== -->
    <?php include("inc/footer.php"); ?>
    <!-- ==========footer-section========== -->


    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/modernizr-3.6.0.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/jquery.ripples-min.js"></script>
    <script src="assets/js/lightcase.js"></script>
    <script src="assets/js/swiper.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/countdown.min.js"></script>
    <script src="assets/js/odometer.min.js"></script>
    <script src="assets/js/viewport.jquery.js"></script>
    <script src="assets/js/nice-select.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>