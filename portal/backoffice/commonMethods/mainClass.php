<?php

    //function to return status by ID
    function getStatus($id){
        include "../../config-files/connectiondb.php";
        $query_get_status = mysqli_query($connection, "SELECT * FROM `system_status` WHERE `status_id`= ".$id."");
        while($data = mysqli_fetch_array($query_get_status)){
            $status_name = $data['status_name'];     
        }
        return $status_name;
    }
    //function to return student details by stud no
    function getStudentBy($username){
        include "../../config-files/connectiondb.php";
        $query_get_user = mysqli_query($connection, "SELECT * FROM `student` WHERE `student_no`= '".$username."'");
        try{
            while($data = mysqli_fetch_array($query_get_user)){
                $studentId = $data['stud_id'];     
            }
            return $studentId;
        }
        catch(exception  $ex){
            return 0;
        }
    }
    //function to return role details by stud no
    function getSystemRoles(){
        include "../../config-files/connectiondb.php";
        $query_get_role = mysqli_query($connection, "SELECT * FROM `role`");
        echo "<select class='selects' name='roles'>";
        echo "<option disabled>Select role</option>";
        while($data = mysqli_fetch_array($query_get_role)){
            echo "<option value=".$data["role_id"].">".$data["role_name"]."</option>";
        }
        echo "</select><br/><br/>";
    }
        //function to return status by ID
    function getRoleById($id){
        include "../../config-files/connectiondb.php";
        $query_get_position = mysqli_query($connection, "SELECT * FROM `role` WHERE `role_id`= ".$id."");
        while($data = mysqli_fetch_array($query_get_position)){
            $position_name = $data['role_name'];     
        }
        return $position_name;
    }
    //Get drivers details
    function getEmployeeDetails($driverId){
        include "../../config-files/connectiondb.php";
        $query_get_emp = mysqli_query($connection, "SELECT * FROM `employee` WHERE `emp_id`= '".$driverId."'");
        try{
            while($data = mysqli_fetch_array($query_get_emp)){
                $employee = $data['first_name']." ".$data['last_name'];     
            }
            return $employee;
        }
        catch(exception  $ex){
            return 0;
        }
    }
    function getBusDetails($busId){
        include "../../config-files/connectiondb.php";
        $query_get_bus = mysqli_query($connection, "SELECT * FROM `sch_bus` WHERE `bus_id`= '".$busId."'");
        try{
            while($data = mysqli_fetch_array($query_get_bus)){
                $busnames = $data['bus_name']." <b>(".$data['bus_plate_no'].")</b>";     
            }
            return $busnames;
        }
        catch(exception  $ex){
            return 0;
        }
    }
    function getCampusDetails($campusId){
        include "../../config-files/connectiondb.php";
        $query_get_bus = mysqli_query($connection, "SELECT * FROM `sch_campus` WHERE `campus_id`= '".$campusId."'");
        try{
            $campusDetails = array();
            if($data = mysqli_fetch_array($query_get_bus)){
                return array($data['campus_name'], $data['campus_description'], $data['location'], $data['position_lat'], $data['position_long'], $data['created_date']);    
            }
        }
        catch(exception  $ex){
            return 0;
        }
    }
    //all drivers
    function getAllDrivers(){
        include "../../config-files/connectiondb.php";
        $query_get_drivers = mysqli_query($connection, "SELECT * FROM `employee`");
        echo "<select class='selects' name='drivers'>";
        echo "<option disabled>Select Driver</option>";
        while($data = mysqli_fetch_array($query_get_drivers)){
            echo "<option value=".$data["emp_id"].">".$data["first_name"]." ".$data["last_name"]." - ".getRoleById($data["position"])."</option>";
        }
        echo "</select>";
    }
    //get all buses
    function getAllBuses(){
        include "../../config-files/connectiondb.php";
        $query_get_bus = mysqli_query($connection, "SELECT * FROM `sch_bus`");
        echo "<select class='selects' name='buses'>";
        echo "<option disabled>Select Bus</option>";
        while($data = mysqli_fetch_array($query_get_bus)){
            echo "<option value=".$data["bus_id"].">".$data["bus_name"]." - ".$data["bus_plate_no"]." (".getStatus($data["travel_status_id"]).")</option>";
        }
        echo "</select>";
    }
    //get all Campuses
    function getAllCampuses($nameId){
        include "../../config-files/connectiondb.php";
        $query_get_campus = mysqli_query($connection, "SELECT * FROM `sch_campus`");
        echo "<select class='selects' name='".$nameId."'>";
        echo "<option disabled>Select Campuses</option>";
        while($data = mysqli_fetch_array($query_get_campus)){
            echo "<option value=".$data["campus_id"].">".$data["campus_name"]." (".$data["location"].")</option>";
        }
        echo "</select>";
    }
        //get all Campuses
    function getAllStatus(){
        include "../../config-files/connectiondb.php";
        $query_get_status = mysqli_query($connection, "SELECT * FROM `system_status`");
        echo "<select class='selects' name='status'>";
        echo "<option disabled>Select System Status</option>";
        while($data = mysqli_fetch_array($query_get_status)){
            echo "<option value=".$data["status_id"].">".$data["status_name"]."</option>";
        }
        echo "</select>";
    }
    function getAssignedBus($assignedId){
        include "../../config-files/connectiondb.php";
        $query_get_assigned_bus = mysqli_query($connection, "SELECT * FROM `bus_assigned_driver` WHERE `bus_assigned_id`= '".$assignedId."'");
        try{
            if($data = mysqli_fetch_array($query_get_assigned_bus)){
                return array($data['bus_assigned_id'], $data['emp_id'], $data['bus_id'], $data['des_from_id'], $data['des_to_id'], $data['status_id'], $data['created_date']);    
            }
        }
        catch(exception  $ex){
            return 0;
        }
    }
    //Get all student details
    function getAllStudentDetails($studId){
        include "../../config-files/connectiondb.php";
        $query_get_all_student_details = mysqli_query($connection, "SELECT * FROM `student` WHERE `stud_id`= '".$studId."'");
        try{
            if($data = mysqli_fetch_array($query_get_all_student_details)){
                return array($data['student_no'], $data['first_name']." ".$data['last_name'], $data['nrc_no'], $data['phone_no'], $data['email_address'], $data['programme_of_study'], $data['mode_of_study'], $data['home_address'], $data['status_id']);    
            }
        }
        catch(exception  $ex){
            return 0;
        }
    }
    //Get employee Details
    function getAllEmpDetails($email){
        include "../../config-files/connectiondb.php";
        $query_get_all_emp_details = mysqli_query($connection, "SELECT * FROM `employee` WHERE `email_address`= '".$email."'");
        try{
            if($data = mysqli_fetch_array($query_get_all_emp_details)){
                return array($data['emp_id'], $data['nrc'], $data['drivers_license'], $data['first_name']." ".$data['last_name'], $data['phone_number'], $data['email_address'], $data['position'], $data['home_address'], $data['status_id']);    
            }
        }
        catch(exception  $ex){
            return 0;
        }
    }
    function findAssignedTaskByRef($referenceNo){
        include "../../config-files/connectiondb.php";
        $query_get_assigned_bus = mysqli_query($connection, "SELECT * FROM `bus_assigned_driver` WHERE `reference_no`= '".$referenceNo."'");
        try{
            if($data = mysqli_fetch_array($query_get_assigned_bus)){
                return array($data['bus_assigned_id'], $data['des_from_id'], $data['des_to_id'], $data['reference_no'], $data['created_by']);    
            }
        }
        catch(exception  $ex){
            return 0;
        }
    }
    //Get array of campus details
    function findCampusById($busLocationId){
        include "../../config-files/connectiondb.php";
        $query_get_campus = mysqli_query($connection, "SELECT * FROM `sch_campus` WHERE `campus_id`= '".$busLocationId."'");
        try{
            if($data = mysqli_fetch_array($query_get_campus)){
                return array($data['campus_id'], $data['campus_name'], $data['campus_description'], $data['position_lat'], $data['position_long']);    
            }
        }
        catch(exception  $ex){
            return 0;
        }

    }
    function getchargeById($id){
        include "../../config-files/connectiondb.php";
        $query_get_charge = mysqli_query($connection, "SELECT * FROM `bus_charge` WHERE `bus_charge_id`= '".$id."'");
        try{
            if($data = mysqli_fetch_array($query_get_charge)){
                return array($data['bus_charge_id'], $data['bus_charge_type'], $data['bus_charge_amount'], $data['bus_charge_status_id'], $data['created_date']);    
            }
        }
        catch(exception  $ex){
            return 0;
        }

    }
?>