<?php
    error_reporting(E_ERROR | E_PARSE);//removes the undefined problem
    session_start();

    include "../../config-files/connectiondb.php";
    $user_login_id = $_SESSION["username"];

    if($user_login_id == ""){
        header("Location:../../index.php");
    }
    $empId = $_GET["id"];
    if(mysqli_query($connection, "DELETE FROM `backoffice_user` WHERE `emp_id` = '".$empId."'")){ 
        if (mysqli_affected_rows($connection) > 0){
            //Delete from employee as well.
            if(mysqli_query($connection, "DELETE FROM `employee` WHERE `emp_id` = '".$empId."'")){
                if(mysqli_affected_rows($connection) > 0){
                    echo '<script>
                        alert("Employee deleted succesfully.");
                        location= "employees-list.php";
                    </script>';
                }
                else{
                    echo '<script>
                        alert("Failed to delete employee from parent table, try again.");
                        location= "employees-list.php";
                    </script>'; 
                }
            } 
        }
        else{
            echo '<script>
                    alert("failed to delete employee from child table, try again.");
                    location= "employees-list.php";
                  </script>'; 
        }              
    }

?>