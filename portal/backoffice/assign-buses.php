<?php
    error_reporting(E_ERROR | E_PARSE);//removes the undefined problem
    session_start();

    include "../../config-files/connectiondb.php";
    require_once "commonMethods/mainClass.php";
    $user_login_id = $_SESSION["username"];

    if($user_login_id == ""){
        header("Location:../../index.php");
    }
    //check pending requests
    $query_get_rtps = mysqli_query($connection, "SELECT COUNT(*) AS 'notification' FROM `student_book_bus` WHERE `status_id`= 1");
    while($data = mysqli_fetch_array($query_get_rtps)){
         $total = $data['notification'];     
    }
    //get user information and prepare for display
    $query_get_details = mysqli_query($connection, "SELECT bk.user_id, bk.username, bk.status_id, bk.role_id, bk.emp_id, em.first_name, em.last_name, em.phone_number, em.email_address, em.home_address, em.nrc, em.drivers_license 
    FROM `backoffice_user` bk, `employee` em
    WHERE bk.emp_id = em.emp_id AND bk.username = '".$user_login_id."'");
    while($row = mysqli_fetch_array($query_get_details)){
        $names = $row['first_name']." ".$row['last_name']; 
        $employee_id = $row["username"];
        $user_role = $row["role_id"];     
    }
    $error = "";    

    if(isset($_POST["signup-button"])){
        $driverId = $_POST["drivers"];
        $busesId = $_POST["buses"];
        $destinationFrom = $_POST["destFrom"];
        $destinationTo = $_POST["destTo"];
        $statusId = $_POST["status"];
        $createdBy = $_SESSION["logged_user"];
        //Generate a reference number
        $referenceNo = date("Y").date("s").date("i").mt_rand();

        if(!empty($driverId) && !empty($busesId) && !empty($destinationFrom) && !empty($destinationTo) && !empty($statusId)){
            if($destinationFrom == $destinationTo){                
                $error = '<span style="color: red;">Destinations can not be the same.</span>';
            }
            else{
                $query_existing_task = mysqli_query($connection, "SELECT * FROM `bus_assigned_driver`  WHERE `emp_id` = '".$driverId."' AND `bus_id` = '".$busesId."' AND `status_id` <> 4");//Task is not

                if($row = mysqli_fetch_array($query_existing_task)){
                                
                    $error = '<span style="color: red;">Driver "'.getEmployeeDetails($driverId).'" already assigned a task.</span>';
                }
                else{                                
                    $query_insert_task = "INSERT INTO `bus_assigned_driver`(`emp_id`, `bus_id`, `des_from_id`, `des_to_id`, `status_id`, `reference_no`, `created_by`) 
                    VALUES ('".$driverId."','".$busesId."','".$destinationFrom."','".$destinationTo."','".$statusId."', '".$referenceNo."','".$createdBy."')";
                    if(mysqli_query($connection, $query_insert_task)){
                        //Get Task by referenec no
                        $bus_location = findAssignedTaskByRef($referenceNo);
                        $campus_from_details = findCampusById($bus_location[1]);
                        $campus_to_details = findCampusById($bus_location[2]);
                        //Query for insert bus location
                        $query_bus_location_from = "INSERT INTO `bus_assigned_location`(`bus_assigned_id`, `latitude`, `longitude`, `status_id`, `created_by`) 
                        VALUES ('".$bus_location[0]."','".$campus_from_details[3]."','".$campus_from_details[4]."','".$statusId."','".$createdBy."')";

                        $query_bus_location_to = "INSERT INTO `bus_assigned_location`(`bus_assigned_id`, `latitude`, `longitude`, `status_id`, `created_by`) 
                        VALUES ('".$bus_location[0]."','".$campus_to_details[3]."','".$campus_to_details[4]."','".$statusId."','".$createdBy."')";

                        if(mysqli_query($connection, $query_bus_location_from) && mysqli_query($connection, $query_bus_location_to)){
                            echo '<script>
                                    alert("User Assigned succesfully.");
                                    location= "all_schedule_list.php";
                                </script>'; 
                        }
                    }                               
                }
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
    
    <title>Admin | Assign Bus</title>
</head>
<body>
    <header>
        <div class="sub-header">
            <div class="contact-holder">  
                <div id="phone-holder">
                    <span><img style="width: 35px; height: 35px; margin-top: -9px;" src="../images/profile.png" alt="profile"/><i><?php echo $names; ?></i></span><br />
                </div>              
                <div id="reg-holder">
                    <?php
                            echo '<span><a href="logout.php" id="apply-id" class="button-clicks">Sign Out</a></span> <span><a href="flaged-reviews.php" id="apply-id" class="button-clicks">Reports <sup><label style="font-weight: bold;">'.$total.'</label></sup></a></span>';

                    ?>
                                        
                </div>
            </div>
        </div>
        <div class="main-header">
            <div class="nav-holder">
                <div id="logo-div">
                    <img src="../../assets/images/logo/logo.jpg" alt="logo">
                </div>
                <nav>
                    <ul>
                        <li><a href="admin-home.php">Dashboard</a></li>
                        <?php
                            echo '<li><a href="clients-list.php">Manage students</a></li>
                            <li><a href="all_schedule_list.php">Schedule</a></li>
                            <li><a href="view-trips.php">Manage Trips</a></li>
                            <li><a href="assign-buses.php">Assign buses</a></li>
                            <li><a href="reports.php">Reports</a></li>'; 
                            
                                        
                        ?>                      
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <section>
        <div class="rolling-images">
        <div class="search-section">            
                <?php echo $error."<br/><br/>"; ?>
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <?php
                        getAllDrivers();
                    ?>
                    <?php
                        getAllBuses();
                    ?>                    
                    <br/><br/>
                    <?php
                        getAllCampuses("destFrom");
                    ?>
                    <?php
                        getAllCampuses("destTo");
                    ?><br/><br/>
                    <?php
                        getAllStatus();
                    ?>
                    <input id="search-id" type="text" name="createdby" class="inputs" value="<?php echo $user_login_id; ?>" readonly style="text-align: center;" Required /><br/><br/>
                    
                    <input id="submit-id" type="submit" name="signup-button" value="Assign"/>
                </form>
            </div>
        </div>
    </section>
    <footer>
        <div>
            <span>&copy; <?php echo date("Y");?> Maambo<br />All Rights Reserved<br/><i>Developed by  Chibozu Maambo</i></span>
        </div>
    </footer>    
</body>
</html>