<?php
    error_reporting(E_ERROR | E_PARSE);//removes the undefined problem
    session_start();

    include "../../config-files/connectiondb.php";
    require_once "commonMethods/mainClass.php";
    $user_login_id = $_SESSION["username"];

    if($user_login_id == ""){
        header("Location:../../index.php");
    }
    //check pending requests
    $query_get_rtps = mysqli_query($connection, "SELECT COUNT(*) AS 'notification' FROM `student_book_bus` WHERE `status_id`= 1");
    while($data = mysqli_fetch_array($query_get_rtps)){
         $total = $data['notification'];     
    }
    //get user information and prepare for display
    $query_get_details = mysqli_query($connection, "SELECT bk.user_id, bk.username, bk.status_id, bk.role_id, bk.emp_id, em.first_name, em.last_name, em.phone_number, em.email_address, em.home_address, em.nrc, em.drivers_license 
    FROM `backoffice_user` bk, `employee` em
    WHERE bk.emp_id = em.emp_id AND bk.username = '".$user_login_id."'");
    while($row = mysqli_fetch_array($query_get_details)){
        $names = $row['first_name']." ".$row['last_name']; 
        $employee_id = $row["username"];
        $user_role = $row["role_id"];     
    }
    $error = "";

    if(isset($_POST["signup-button"])){
        $campusname = $_POST["campus_name"];
        $desc = $_POST["description"];
        $locationId = $_POST["physical_address"];
        $lat = $_POST["latitude"];
        $long = $_POST["longitude"];
        $createdBy = $_SESSION["logged_user"];

        if(!empty($campusname) && !empty($desc) && !empty($locationId) && !empty($lat) && !empty($long)){
            $query_existing_campus = mysqli_query($connection, "SELECT * FROM `sch_campus`  WHERE `campus_name` = '".$campusname."'");

            if($row = mysqli_fetch_array($query_existing_campus)){
                              
                $error = '<span style="color: red;">Campus with name "'.$campusname.'" already in the system</span>';
            }
            else{                                
                $query_insert_campus = "INSERT INTO `sch_campus`(`campus_name`, `campus_description`, `location`, `position_lat`, `position_long`, `created_by`) 
                VALUES ('".$campusname."','".$desc."','".$locationId."','".$lat."','".$long."','".$createdBy."')";
                if(mysqli_query($connection, $query_insert_campus)){
                    
                    header("Location:view-campus.php");  
                }                               
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
    
    <title>Portal | Add Campus</title>
</head>
    <header>
        <div class="sub-header">
            <div class="contact-holder">  
                <div id="phone-holder">
                    <span><img style="width: 35px; height: 35px; margin-top: -9px;" src="../images/profile.png" alt="profile"/><i><?php echo $names; ?></i></span><br />
                </div>              
                <div id="reg-holder">
                    <?php
                            echo '<span><a href="logout.php" id="apply-id" class="button-clicks">Sign Out</a></span> <span><a href="flaged-reviews.php" id="apply-id" class="button-clicks">Reports <sup><label style="font-weight: bold;">'.$total.'</label></sup></a></span>';

                    ?>
                                        
                </div>
            </div>
        </div>
        <div class="main-header">
            <div class="nav-holder">
                <div id="logo-div">
                    <img src="../../assets/images/logo/logo.jpg" alt="logo">
                </div>
                <nav>
                    <ul>
                        <li><a href="admin-home.php">Dashboard</a></li>
                        <?php
                            echo '<li><a href="clients-list.php">Manage students</a></li>
                            <li><a href="all_schedule_list.php">Schedule</a></li>
                            <li><a href="view-trips.php">Manage Trips</a></li>
                            <li><a href="assign-buses.php">Assign buses</a></li>
                            <li><a href="reports.php">Reports</a></li>';                         
                                        
                        ?>                      
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <section>
        <div class="rolling-images">
        <div class="search-section">            
                <?php echo $error."<br/><br/>"; ?>
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <input id="search-id" type="text" name="campus_name" class="inputs" placeholder="Enter Campus Name.." style="text-align: center;" Required />
                    <input id="search-id" type="text" name="description" class="inputs" placeholder="Campus description.." style="text-align: center;" Required />
                    <br/><br/>
                    <input id="search-id" type="text" name="physical_address" class="inputs" placeholder="Enter Physical address.." style="text-align: center;" Required />
                    <input id="search-id" type="text" name="latitude" class="inputs" placeholder="Enter Campus Latitude.." style="text-align: center;" Required />
                    <br/><br/>
                    <input id="search-id" type="text" name="longitude" class="inputs" placeholder="Enter Campus Longitude.." style="text-align: center;" Required />
                    <input id="search-id" type="text" name="current_user" class="inputs" style="text-align: center;" value="<?php echo $user_login_id; ?>" readonly/><br/><br/>
                    <input id="submit-id" type="submit" name="signup-button" value="Submit"/>
                </form>
            </div>
        </div>
    </section> 

    <footer>
        <div>
            <span>&copy; <?php echo date("Y");?> Maambo<br />All Rights Reserved<br/><i>Developed by  Chibozu Maambo</i></span>
        </div>
    </footer>    
</body>
</html>