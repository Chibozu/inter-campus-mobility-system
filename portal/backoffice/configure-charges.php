<?php
    error_reporting(E_ERROR | E_PARSE);//removes the undefined problem
    session_start();

    include "../../config-files/connectiondb.php";
    require_once "commonMethods/mainClass.php";
    $user_login_id = $_SESSION["username"];

    if($user_login_id == ""){
        header("Location:../../index.php");
    }
    //check pending requests
    $query_get_rtps = mysqli_query($connection, "SELECT COUNT(*) AS 'notification' FROM `student_book_bus` WHERE `status_id`= 1");
    while($data = mysqli_fetch_array($query_get_rtps)){
         $total = $data['notification'];     
    }
    //get user information and prepare for display
    $query_get_details = mysqli_query($connection, "SELECT bk.user_id, bk.username, bk.status_id, bk.role_id, bk.emp_id, em.first_name, em.last_name, em.phone_number, em.email_address, em.home_address, em.nrc, em.drivers_license 
    FROM `backoffice_user` bk, `employee` em
    WHERE bk.emp_id = em.emp_id AND bk.username = '".$user_login_id."'");
    while($row = mysqli_fetch_array($query_get_details)){
        $names = $row['first_name']." ".$row['last_name']; 
        $employee_id = $row["username"];
        $user_role = $row["role_id"];     
    }
    $error = "";

    if(isset($_POST["signup-button"])){
        $type = $_POST["chargeType"];
        $amount = $_POST["amountId"];
        $status = 1;
        $createdBy = $_SESSION["logged_user"];

        if(!empty($type) && !empty($amount)){
            $query_existing_charge = mysqli_query($connection, "SELECT * FROM `bus_charge` WHERE `bus_charge_type` = '".$type."'");

            if($row = mysqli_fetch_array($query_existing_charge)){
                              
                $error = '<span style="color: red;">charge type "'.$type.'" already in the system</span>';
            }
            else{                                
                $query_insert_charge = "INSERT INTO `bus_charge`(`bus_charge_type`, `bus_charge_amount`, `bus_charge_status_id`, `created_by`) 
                VALUES ('".$type."','".$amount."','".$status."','".$createdBy."')";
                if(mysqli_query($connection, $query_insert_charge)){
                    echo '<script>
                            alert("charge added succesfully.");
                            location= "view-charges.php";
                          </script>';
                }                               
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
    
    <title>Portal | Add Bus</title>
</head>
<body>
    <header>
        <div class="sub-header">
            <div class="contact-holder">  
                <div id="phone-holder">
                    <span><img style="width: 35px; height: 35px; margin-top: -9px;" src="../images/profile.png" alt="profile"/><i><?php echo $names; ?></i></span><br />
                </div>              
                <div id="reg-holder">
                    <?php
                            echo '<span><a href="logout.php" id="apply-id" class="button-clicks">Sign Out</a></span> <span><a href="flaged-reviews.php" id="apply-id" class="button-clicks">Reports <sup><label style="font-weight: bold;">'.$total.'</label></sup></a></span>';

                    ?>
                                        
                </div>
            </div>
        </div>
        <div class="main-header">
            <div class="nav-holder">
                <div id="logo-div">
                    <img src="../../assets/images/logo/logo.jpg" alt="logo">
                </div>
                <nav>
                    <ul>
                        <li><a href="admin-home.php">Dashboard</a></li>
                        <?php
                            echo '<li><a href="clients-list.php">Manage students</a></li>
                            <li><a href="all_schedule_list.php">Schedule</a></li>
                            <li><a href="view-trips.php">Manage Trips</a></li>
                            <li><a href="assign-buses.php">Assign buses</a></li>
                            <li><a href="reports.php">Reports</a></li>';                         
                                        
                        ?>                      
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <section>
        <div class="rolling-images">
        <div class="search-section">            
                <?php echo $error."<br/><br/>"; ?>
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">                    
                    <select class='selects' name='chargeType'>
                        <option disabled>Select Charge type</option>
                        <option value="Daily">Daily</option>
                        <option value="Weekly">Weekly</option>
                        <option value="Monthly">Monthly</option>
                        <option value="Semester">Semester</option>                        
                    </select>
                    <input id="search-id" type="number" name="amountId" class="inputs" placeholder="Enter Amount.." min=0 style="text-align: center;" Required />
                    <br/><br/>
                    
                    <input id="submit-id" type="submit" name="signup-button" value="Submit"/>
                </form>
            </div>
        </div>
    </section>
    <footer>
        <div>
            <span>&copy; <?php echo date("Y");?> Maambo<br />All Rights Reserved<br/><i>Developed by  Chibozu Maambo</i></span>
        </div>
    </footer>    
</body>
</html>