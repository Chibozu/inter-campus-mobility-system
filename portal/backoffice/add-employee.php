<?php
    error_reporting(E_ERROR | E_PARSE);//removes the undefined problem
    session_start();

    include "../../config-files/connectiondb.php";
    require_once "commonMethods/mainClass.php";
    $user_login_id = $_SESSION["username"];

    if($user_login_id == ""){
        header("Location:../../index.php");
    }
    //check pending requests
    $query_get_rtps = mysqli_query($connection, "SELECT COUNT(*) AS 'notification' FROM `student_book_bus` WHERE `status_id`= 1");
    while($data = mysqli_fetch_array($query_get_rtps)){
         $total = $data['notification'];     
    }
    //get user information and prepare for display
    $query_get_details = mysqli_query($connection, "SELECT bk.user_id, bk.username, bk.status_id, bk.role_id, bk.emp_id, em.first_name, em.last_name, em.phone_number, em.email_address, em.home_address, em.nrc, em.drivers_license 
    FROM `backoffice_user` bk, `employee` em
    WHERE bk.emp_id = em.emp_id AND bk.username = '".$user_login_id."'");
    while($row = mysqli_fetch_array($query_get_details)){
        $names = $row['first_name']." ".$row['last_name']; 
        $employee_id = $row["username"];
        $user_role = $row["role_id"];     
    }
    $error_message = "";

    if(isset($_POST["signup-button"])){

        $nrc_number = $_POST["nrc_id"];
        $license_no = $_POST["drivers_license"];
        $firstName = $_POST["first_name"];
        $lastName = $_POST["last_name"];
        $number = $_POST["phone_no"];
        $mail = $_POST["email_address"];
        $address = $_POST["home_address"];
        $statusId = 1;//Active by default
        $roleId = $_POST["roles"];
        $createdBy = $_SESSION["logged_user"];
        $pass_key = $_POST["password"];
        $confirm_pass_key = $_POST["con_password"];

        if(!empty($nrc_number) || !empty($license_no) || !empty($firstName) || !empty($lastName) || !empty($mail) || !empty($number) || !empty($address) || !empty($confirm_pass_key)){
            if(filter_var($mail, FILTER_VALIDATE_EMAIL)){
                if($pass_key == $confirm_pass_key){
                    if(strlen($pass_key) >= 4){
                        $hasshed_password = MD5($pass_key);
                        $hashed_confirm_password = MD5($confirm_pass_key);

                        $query_existing_emp = mysqli_query($connection, "SELECT bk.user_id, bk.username, bk.status_id, bk.role_id, bk.emp_id, em.first_name, em.last_name, em.phone_number, em.email_address, em.home_address, em.nrc, em.drivers_license 
                        FROM `backoffice_user` bk, `employee` em
                        WHERE bk.emp_id = em.emp_id AND (em.nrc = '".$nrc_number."' OR bk.username = '".$mail."' OR em.drivers_license = '".$license_no."')");

                        if($row = mysqli_fetch_array($query_existing_emp)){
                              
                            $error_message = '<b style="color: red;">User with Username "'.$mail.'" already in the system, <a href="sign-in.php"> Login</a></b>';
                        }
                        else{     
                            $query_insert_emp = "INSERT INTO `employee`(`nrc`, `drivers_license`, `first_name`, `last_name`, `phone_number`, `email_address`, `position`, `home_address`, `status_id`, `created_by`) 
                            VALUES ('".$nrc_number."','".$license_no."','".$firstName."','".$lastName."','".$number."','".$mail."','".$roleId."','".$address."','".$statusId."','".$createdBy."')";                       
                            if(mysqli_query($connection, $query_insert_emp)){
                                //Get Employe email and save employee to backoffice table
                                $getEmployeeId = getAllEmpDetails($mail);
                                $backoffice_user_query = "INSERT INTO `backoffice_user`(`username`, `status_id`, `role_id`, `password`, `emp_id`, `created_by`, `created_date`, `modified_by`, `modified_date`) 
                                VALUES ('".$mail."','".$statusId."','".$roleId."','".$hasshed_password."','".$getEmployeeId[0]."','[value-7]','[value-8]','[value-9]','[value-10]')";
                                
                                if(mysqli_query($connection, $backoffice_user_query)){
                                    $error_message = '<b style="color: #006600;">User '.$firstName.' has been added.</b>';
                                   // $_SESSION["error_sending"] = '<b style="color: #00FF00;">User '.$firstName.' has been added, but email not sent</b>';
                                    //Header("Location:../../sendMail.php?user=".$studentNo."&names=".$firstName." ".$lastName."&email=".$mail."&password=".$pass_key."");
                                }
                            }
                            else{
                                $error_message = '<b style="color: #00FF00;">Failed to add user, '.$mail.'. Try again</b>';
                            }                            
                                
                        }
                    }
                    else{
                        $error_message = "<b style='color: #FF0000;'>Password should be atleast 4 characters</b>";
                    }
                }
                else{
                    $error_message = "<b style='color: #FF0000;'>Your password did not match</b>"; 
                }

            }
            else{
                $error_message = "<b style='color: #FF0000;'>Email address not valid</b>"; 
            }
        }
        else{
            $error_message = "<b style='color: #FF0000;'>Note: All fields are required</b>";
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
    
    <title>Admin | Add Employee</title>
</head>
<body>
    <header>
        <div class="sub-header">
            <div class="contact-holder">  
                <div id="phone-holder">
                    <span><img style="width: 35px; height: 35px; margin-top: -9px;" src="../images/profile.png" alt="profile"/><i><?php echo $names; ?></i></span><br />
                </div>              
                <div id="reg-holder">
                    <?php
                            echo '<span><a href="logout.php" id="apply-id" class="button-clicks">Sign Out</a></span> <span><a href="flaged-reviews.php" id="apply-id" class="button-clicks">Reports <sup><label style="font-weight: bold;">'.$total.'</label></sup></a></span>';

                    ?>
                                        
                </div>
            </div>
        </div>
        <div class="main-header">
            <div class="nav-holder">
                <div id="logo-div">
                    <img src="../../assets/images/logo/logo.jpg" alt="logo">
                </div>
                <nav>
                    <ul>
                    <li><a href="admin-home.php">Dashboard</a></li>
                        <?php
                            echo '<li><a href="clients-list.php">Manage students</a></li>
                            <li><a href="all_schedule_list.php">Schedule</a></li>
                            <li><a href="view-trips.php">Manage Trips</a></li>
                            <li><a href="assign-buses.php">Assign buses</a></li>
                            <li><a href="reports.php">Reports</a></li>';                            
                                        
                        ?>             
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <section>
        <div class="rolling-images">
        <div class="search-section">  
            <span><?php echo $error_message; ?></span><br /><br />         
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <input id="search-id" type="text" name="nrc_id" class="inputs" placeholder="Enter Nrc No.." style="text-align: center;" Required />
                <input id="search-id" type="text" name="drivers_license" class="inputs" placeholder="Enter Drivers License.." style="text-align: center;" Required /><br/><br/>
                <input id="search-id" type="text" name="first_name" class="inputs" placeholder="Enter Firstname.." style="text-align: center;" Required />
                <input id="search-id" type="text" name="last_name" class="inputs" placeholder="Enter Lastname.." style="text-align: center;" Required /><br/><br/>
                <input id="search-id" type="text" name="phone_no" class="inputs" placeholder="Enter Phone No.." style="text-align: center;" Required />
                <input id="search-id" type="text" name="email_address" class="inputs" placeholder="Enter Email Address.." style="text-align: center;" Required /><br/><br/>
                <input id="search-id" type="text" name="home_address" class="inputs" placeholder="Enter Home address.." style="text-align: center;" Required />
                <?php
                   getSystemRoles();
                ?>
                <input id="search-id" type="password" name="password" class="inputs" placeholder="Enter Password.." style="text-align: center;" Required />
                <input id="search-id" type="password" name="con_password" class="inputs" placeholder="Confirm Password.." style="text-align: center;" Required /><br/><br/>
                    
                <input id="submit-id" type="submit" name="signup-button" value="Register"/>                    
                </form>
            </div>
        </div>
    </section>
    <br/><br/>
    <footer>
        <div style="background-color: #006600; text-align: center; color: #FFFFFF; margin-top: 10%"><p><span>&copy;&nbsp;</span><span class="copyright-year"></span><?php echo date("Y"); ?> All Rights Reserved<br />Developed by<a href="#" target="_blank" style = "color: Orange; "> <i> Chibozu Maambo</i></a><br />Email: chibozu.maambo@gmail.com</p></div>
    </footer>     
</body>
</html>