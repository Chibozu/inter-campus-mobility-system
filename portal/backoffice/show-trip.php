<?php
    error_reporting(E_ERROR | E_PARSE);//removes the undefined problem
    session_start();

    include "../../config-files/connectiondb.php";
    require_once "commonMethods/mainClass.php";
    $user_login_id = $_SESSION["username"];

    if($user_login_id == ""){
        header("Location:../../index.php");
    }
    //check pending requests
    $query_get_rtps = mysqli_query($connection, "SELECT COUNT(*) AS 'notification' FROM `student_book_bus` WHERE `status_id`= 1");
    while($data = mysqli_fetch_array($query_get_rtps)){
         $total = $data['notification'];     
    }
    //get user information and prepare for display
    $query_get_details = mysqli_query($connection, "SELECT bk.user_id, bk.username, bk.status_id, bk.role_id, bk.emp_id, em.first_name, em.last_name, em.phone_number, em.email_address, em.home_address, em.nrc, em.drivers_license 
    FROM `backoffice_user` bk, `employee` em
    WHERE bk.emp_id = em.emp_id AND bk.username = '".$user_login_id."'");
    while($row = mysqli_fetch_array($query_get_details)){
        $names = $row['first_name']." ".$row['last_name']; 
        $employee_id = $row["username"];
        $user_role = $row["role_id"];     
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
    
    <title>Admin | Bus List</title>
</head>
<body>
    <header>
        <div class="sub-header">
            <div class="contact-holder">  
                <div id="phone-holder">
                    <span><img style="width: 35px; height: 35px; margin-top: -9px;" src="../images/profile.png" alt="profile"/><i><?php echo $names; ?></i></span><br />
                </div>              
                <div id="reg-holder">
                    <?php
                            echo '<span><a href="logout.php" id="apply-id" class="button-clicks">Sign Out</a></span> <span><a href="flaged-reviews.php" id="apply-id" class="button-clicks">Reports <sup><label style="font-weight: bold;">'.$total.'</label></sup></a></span>';

                    ?>
                                        
                </div>
            </div>
        </div>
        <div class="main-header">
            <div class="nav-holder">
                <div id="logo-div">
                    <img src="../../assets/images/logo/logo.jpg" alt="logo">
                </div>
                <nav>
                    <ul>
                    <li><a href="admin-home.php">Dashboard</a></li>
                        <?php
                            echo '<li><a href="clients-list.php">Manage students</a></li>
                            <li><a href="all_schedule_list.php">Schedule</a></li>
                            <li><a href="view-trips.php">Manage Trips</a></li>
                            <li><a href="assign-buses.php">Assign buses</a></li>
                            <li><a href="reports.php">Reports</a></li>';                                    
                        ?>                    
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <section>
        <div class="rolling-images">
            <?php
                $coordinates = array();
                $latitudes = array();
                $longitudes = array();
        
                // Select all the rows in the markers table
                $query = mysqli_query($connection, "SELECT  `latitude`, `longitude` FROM `bus_assigned_location` WHERE `bus_assigned_id` = '".$_GET["id"]."'");
                while($row = mysqli_fetch_array($query)){
                    $latitudes[] = $row['latitude'];
                    $longitudes[] = $row['longitude'];
                    $coordinates[] = 'new google.maps.LatLng(' . $row['latitude'] .','. $row['longitude'] .'),';    
                }            
                //remove the comaa ',' from last coordinate
                $lastcount = count($coordinates)-1;
                $coordinates[$lastcount] = trim($coordinates[$lastcount], ",");

            ?>
            <div id="map" style="width: 100%; height: 80vh;"></div>

        <script>
            function initMap() {
            var mapOptions = {
                zoom: 12,
                center: {<?php echo'lat:'. $latitudes[0] .', lng:'. $longitudes[0] ;?>}, //{lat: --- , lng: ....}
                mapTypeId: google.maps.MapTypeId.SATELITE
            };

            var map = new google.maps.Map(document.getElementById('map'),mapOptions);

            var RouteCoordinates = [
                <?php
                    $i = 0;
                    while ($i < count($coordinates)) {
                        echo $coordinates[$i];
                        $i++;
                    }
                ?>
            ];

            var RoutePath = new google.maps.Polyline({
                path: RouteCoordinates,
                geodesic: true,
                strokeColor: '#ff0000',
                strokeOpacity: 1.0,
                strokeWeight: 8
            });

            mark = '../../assets/images/map/mark.png';
            flag = '../../assets/images/map/flag.png';

            startPoint = {<?php echo'lat:'. $latitudes[0] .', lng:'. $longitudes[0] ;?>};
            endPoint = {<?php echo'lat:'.$latitudes[$lastcount] .', lng:'. $longitudes[$lastcount] ;?>};

            var marker = new google.maps.Marker({
                position: startPoint,
                map: map,
                icon: mark,
                title:"Start point!",
                animation: google.maps.Animation.BOUNCE
            });

            var marker = new google.maps.Marker({
            position: endPoint,
            map: map,
            icon: flag,
            title:"End point!",
            animation: google.maps.Animation.DROP
            });

            RoutePath.setMap(map);
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>

        <!--remenber to put your google map key-->
        <script async defer src= "https://maps.googleapis.com/maps/api/js?key=AIzaSyBvs_QlfrFCxaHysypPYgnDm8hYC92ACzA&callback&callback=initMap"></script>

        </div>
    </section>
    <footer>
        <div style="background-color: #006600; text-align: center; color: #FFFFFF;"><p><span>&copy;&nbsp;</span><span class="copyright-year"></span><?php echo date("Y"); ?> All Rights Reserved<br />Developed by<a href="#" target="_blank" style = "color: Orange; "> <i> Chibozu Maambo</i></a><br />Email: chibozu.maambo@gmail.com</p></div>
    </footer>   
</body>
</html>