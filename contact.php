<!DOCTYPE html>
<html lang="en-us">


<!-- contact.html  22 Nov 2019 04:26:19 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/lightcase.css">
    <link rel="stylesheet" href="assets/css/odometer.css">
    <link rel="stylesheet" href="assets/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/main.css">

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">

    <title>Check Mart | Contact Us</title>



</head>

<body>
    <!-- ==========Preloader========== -->
    <div class="preloader">
        <div class="preloader-wrapper">
            <img src="assets/css/loaders.gif" alt="car-loader">
        </div>
    </div>
    <!-- ==========Preloader========== -->

    <!-- ==========scrolltotop========== -->
    <a href="#0" class="scrollToTop" title="ScrollToTop">
        <img src="assets/images/rocket.png" alt="rocket">
    </a>
    <!-- ==========scrolltotop========== -->

    <!-- ==========header-section========== -->
    <?php include("inc/header.php"); ?>

    <!-- ===========Header Cart=========== -->
    <div id="body-overlay" class="body-overlay"></div>

    <section class="hero-section">
        <div class="hero-area bg_img" data-background="assets/images/banner/checkmart-background.jpg">
            <div class="container">
                <h1 class="title">contact us</h1>
            </div>
        </div>
        <div class="container">
            <ul class="breadcrumb">
                <li>
                    <a href="index.php">home</a>
                </li>
                <li>
                    contact
                </li>
            </ul>
        </div>
    </section>
    <!-- ==========hero-area========== -->

    <!-- ==========Vector-Maps Section========== -->
    <section class="vector-maps-section padding-bottom padding-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="vmap">
                        <div class="position-6">
                            <span class="dot"></span>
                            <div class="details">
                                <h6 class="name">Tuleteka  Road</h6>
                                <p class="area">Rhodes Park Area - Lusaka</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="tab contact-tab">
                        <ul class="tab-menu">
                            <li>Lusaka(lsk)</li>
                        </ul>
                        <div class="tab-area">
                            <div class="tab-item">
                                <div class="Lusaka">
                                    <p>Checkmart Security and Travel Tours is an emerging Security and car rental Company that is owned by Local Zambians.  It was established to specifically deliver quality services to its customers and meet demanding market.</p>
                                    <div class="address-area">
                                        <div class="address-item">
                                            <div class="icon">
                                                <i class="flaticon-placeholder"></i>
                                            </div>
                                            <div class="content">
                                                <h4 class="title">Address</h4>
                                                <ul>
                                                    <li>Tuleteka  Road</li>
                                                    <li>Rhodes Park Area</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="address-item">
                                            <div class="icon">
                                                <i class="flaticon-paper-plane"></i>
                                            </div>
                                            <div class="content">
                                                <h4 class="title">Email Address</h4>
                                                <ul>
                                                    <li>
                                                        <a href="Mailto:checkmart8@gmail.com">checkmart8@gmail.com</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="address-item">
                                            <div class="icon">
                                                <i class="flaticon-phone-call"></i>
                                            </div>
                                            <div class="content">
                                                <h4 class="title">Mobile</h4>
                                                <ul>
                                                    <li>
                                                        <a href="Tel:+260977624948">+260 977 624 948</a>
                                                    </li>                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ==========Vector-Maps Section========== -->

    <!-- ==========Contact-Section========== -->
    <section class="contact-section">
        <div class="container-fluid p-0">
            <div class="row m-0">
                <div class="col-lg-6 p-0">
                    <div class="w-100 h-100 overview-event bg-ash padding-bottom padding-top">
                        <div class="content">
                            <div class="section-header left-style mw-100 mb-low">
                                <p>For any enquiries kindly fill in the form below</p>
                            </div>
                            <form class="contact-form" id="contact_form_submit">
                                <div class="form-group">
                                    <input type="text" placeholder="Name" id="name" name="name">
                                </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Email" id="email" name="email">
                                </div>
                                <div class="form-group w-100">
                                    <textarea id="message"  placeholder="Type Message"></textarea>
                                </div>
                                <div class="form-group w-100">
                                    <input type="submit" value="Send Message">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br/>

    <!-- ==========footer-section========== -->
    <?php include("inc/footer.php"); ?>
    <!-- ==========footer-section========== -->

    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/modernizr-3.6.0.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/jquery.ripples-min.js"></script>
    <script src="assets/js/lightcase.js"></script>
    <script src="assets/js/swiper.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/countdown.min.js"></script>
    <script src="assets/js/odometer.min.js"></script>
    <script src="assets/js/viewport.jquery.js"></script>
    <script src="assets/js/nice-select.js"></script>
    <script src="assets/js/contact.js"></script>
    <script src="assets/js/jquery.vmap.min.js"></script>
    <script src="assets/js/jquery.vmap.world.js"></script>
    <script src="assets/js/main.js"></script>

    <script>
        jQuery(document).ready(function () {
            jQuery('#vmap').vectorMap({
                map: 'world_en',
                color: '#ededff',
                hoverOpacity: 1,
                backgroundColor: 'transparent',
                selectedColor: '#ffca24',
                scaleColors: ['#f7fcff', '#f7fcff'],
                normalizeFunction: 'polynomial'
            });
        });
    </script>
</body>


<!-- contact.html  22 Nov 2019 04:26:20 GMT -->
</html>